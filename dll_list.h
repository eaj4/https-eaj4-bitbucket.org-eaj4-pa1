#ifndef DLL_LIST_H
#define DLL_LIST_H



typedef struct {
	char month[4];
	int day, year;
} dateOfBirth;

typedef struct student {
  int age;
  float gpa;
  int cuid;
  dateOfBirth dob;
  char lastName[20];
  char firstName[15];
  struct student *next;
} student_t;

typedef struct list {
  student_t *head;
  student_t *tail;
  int size;
} list_t;


list_t *newList( );
student_t *addStudent( FILE* inFile, int whichInput );
int printMenu( );
void initializeList( list_t *list, FILE* inFile  );
void addToFront( list_t *list, int whichInput, FILE* inFile );
void addToRear( list_t *list, int whichInput, FILE* inFile );
void deleteStudent( list_t *list, int idNum );
void sortListByLN( list_t *list );
void sortListByNum( list_t *list );
int size( list_t *list );
int isEmpty( list_t *list );
void printStudents ( list_t *list );


#endif /* DLL_LIST_H */
