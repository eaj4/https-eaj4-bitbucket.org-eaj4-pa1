
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h> 
#include "dll_list.h"


list_t *newList( ){

	//variables
	list_t *undergrad = NULL;

	//allocate memory!
	undergrad = (list_t*)malloc(sizeof(list_t*));
	//Check if allocated properly 
	if(undergrad==NULL){
		//print error if not allocated and exit the program. 
		printf("memory allocation falure.\n\n");
		exit(-1);
	}

	//initalize values of undergrad
	undergrad->head = NULL;
	undergrad->tail = NULL;
	undergrad->size = 0;

	//allocate memory for the head/tail pointers
	undergrad->head = (student_t*) malloc (sizeof(student_t*));
	undergrad->tail = (student_t*) malloc (sizeof(student_t*));	
	
	if ((undergrad->head == NULL)||(undergrad->tail ==NULL)){
		printf("node allocation failure\n");
		exit (-1);
	}

return undergrad;	
}




student_t *addStudent(FILE* inFile, int whichInput){

	//variables
	student_t *newNode = NULL;

	//allocate memory & check
	newNode = (student_t*) malloc (sizeof(student_t*));
	if (newNode == NULL){
		printf("node allocation failure\n");
		exit (-1);
	}

	//scan in from file if whichInput is greater or equal to 0
	if (whichInput >= 0){
		fscanf(inFile, "%d", &newNode->age);
		fscanf(inFile, "%f", &newNode->gpa);
		fscanf(inFile, "%d", &newNode->cuid);
		fscanf(inFile, "%s", newNode->dob.month);
		fscanf(inFile, "%d", &newNode->dob.day);
		fscanf(inFile, "%d", &newNode->dob.year);
		fscanf(inFile, "%s", newNode->lastName);
		fscanf(inFile, "%s", newNode->firstName);
	}
	//scan in from user input if whichInput is a negative number
	if(whichInput < 0 ){
		printf("Enter age: ");
		scanf("%d", &newNode->age);

		printf("Enter gpa: ");
		scanf("%f", &newNode->gpa);

		printf("Enter cuid: ");
		scanf("%d", &newNode->cuid);

		printf("Enter 3 letter birth month: ");
		scanf("%s", newNode->dob.month);

		printf("Enter birth day: ");
		scanf("%d", &newNode->dob.day);

		printf("Enter birth year: ");
		scanf("%d", &newNode->dob.year);

		printf("Enter last name: ");
		scanf("%s", newNode->lastName);

		printf("Enter first name: ");
		scanf("%s", newNode->firstName);
		printf("\n\n");
	}
	
return newNode;
}




int printMenu(){
	//variables
	int userChoice = 0;
	bool validNum = false;


	//loop till the user enters a valid response. 
	while(validNum == false){
	
		//print the menu and choices to user
		printf("1. initialize list of students\n");
		printf("2. add additional student to front of list\n");
		printf("3. add additional student to rear of list\n");
		printf("4. delete student\n");
		printf("5. sort students alphabetically\n");
		printf("6. sort students by cuid #\n");
		printf("7. show number of students in list\n");
		printf("8. print students\n");
		printf("9. quit program\n");

		scanf("%d", &userChoice);
		printf("\n");

	
		//test to see if user entered a valid number
		if (userChoice > 0 && userChoice <= 9){
			//true for valid number to be sent to main
			//to exit the loop
			validNum = true;
			printf("- - > %d\n\n", userChoice);
			
		}
		
		//error message to user if not valid number
		else{
			printf("please choose a valid number\n\n");
		}
	}
return userChoice;
}

void initializeList( list_t *list, FILE* inFile ){
	
	int tempSize = 0;
	//read first value in file, # of students in the list
	fscanf(inFile, "%d", &tempSize);
		
	//loop to read in all the students in the file and put them into the list
	for(int i = 0; i < tempSize; ++i){
		addToFront(list, i, inFile);
	}
//FIXME take these out after testing
	/*	printf("%s ", list->head->next->firstName);
	printf("%s ", list->head->next->lastName);
	printf("%d ", list->head->next->age);
	printf("%f ", list->head->next->gpa);
	printf("%d ", list->head->next->cuid);
	printf("%s ", list->head->next->dob.month);
	printf("%d ", list->head->next->dob.day);
	printf("%d ", list->head->next->dob.year);
	printf("\n");*/
//FIXME working correct here but list not going back to main!

	fclose(inFile);

}

void addToFront(list_t *list, int whichInput, FILE* inFile){
	//variables
	student_t *newStudent = NULL;

	//call add student function
	newStudent = addStudent(inFile, whichInput);//correctly running

//FIXME dont need these take out after testing
	/*printf("%s ", newStudent->firstName);
	printf("%s ", newStudent->lastName);
	printf("%d ", newStudent->age);
	printf("%f ", newStudent->gpa);
	printf("%d ", newStudent->cuid);
	printf("%s ", newStudent->dob.month);
	printf("%d ", newStudent->dob.day);
	printf("%d ", newStudent->dob.year);
	printf("\n");*/


//if list size is zero we have to point to head and tail pointers
		if (list->size == 0){
			list->head->next = newStudent;			
			newStudent->next = list->tail;
			++list->size;
		}
		else{
			newStudent->next = list->head->next;
			list->head->next = newStudent;
			++list->size;
		}
}
	




void addToRear(list_t *list, int whichInput, FILE* inFile){
	//variables
	student_t *lastStudent = NULL;

	//call add student function
	lastStudent = addStudent(inFile, whichInput);

//tail next points to last student, last next is null bc at the end, tail = new student.
//FIXME needs testing
	list->tail->next = lastStudent;
	lastStudent->next = NULL;
	list->tail = lastStudent;

}


void deleteStudent(list_t *list, int idNum){

	student_t *tempStudent=NULL;
	tempStudent = list->head->next;

	int count =0;
	for( int i = 0; i < list->size; ++i){
		if(tempStudent->cuid == idNum){
			//delete the student
			//maybe have count to do to delete farther in the list. 
			--list->size;
		}
		tempStudent = tempStudent->next;
		++count;
	}

}

void printStudents(list_t *list){
	student_t *tempStudent = NULL;
	tempStudent = list->head->next;
	
//FIXME isnt working but I think it has to do with values being sent to main!

	//loop for how many are in the list.size
	for (int i =0; i < list->size; ++i){
		printf("%10s,", tempStudent->lastName);
		printf("%20s,", tempStudent->firstName);
		printf("%5d,", tempStudent->age);
		printf("%15d,", tempStudent->cuid);
		printf("%4.2f,", tempStudent->gpa);
		printf("%6s", tempStudent->dob.month);
		printf("%4d,", tempStudent->dob.day);
		printf("%6d\n", tempStudent->dob.year);
		
		tempStudent = tempStudent->next;
	}
	free(tempStudent);


}

